#ifndef _WFG_H_
#define _WFG_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef double OBJECTIVE;

typedef struct
{
    OBJECTIVE *objectives;
} POINT;

typedef struct
{
    int nPoints;
    int n;
    POINT *points;
} FRONT;

typedef struct
{
    int nFronts;
    FRONT *fronts;
} FILECONTENTS;

typedef struct
{
    FRONT iFront;
    int i;
} CURRENTFRONT;

FILECONTENTS *readFile(char[]);

extern void printContents(FILECONTENTS *);

#endif
